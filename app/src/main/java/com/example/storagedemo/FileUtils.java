package com.example.storagedemo;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.InputStream;

/**
 * The author：dp
 * Date：2021/12/6 17:25
 * Des:
 */
public class FileUtils {
    /**
     *  需要权限来访问的文件：媒体文件和文档/其它文件  -->共享目录和其他文件目录
     *
     *  访问的目的是为了拿到路径  路径的获取方式有两种：1.直接拼接 2.mediaStore--->获取到路径或者uri
     */


    /**
     * 1.拼接的方式获取路径
     */
    private void testShareMedia() {
        //获取目录：/storage/emulated/0/
        File rootFile = Environment.getExternalStorageDirectory();
        String imagePath = rootFile.getAbsolutePath() + File.separator + Environment.DIRECTORY_PICTURES + File.separator + "myPic.png";
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
    }


    /**
     * 2.mediaStore方式获取路径
     */
    private void getImagePath(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
        while(cursor.moveToNext()) {
            String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            break;
        }
    }

    /**
     * 2.mediaStore方式获取uri
     */
    private void getImagePath2(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
        while(cursor.moveToNext()) {
            //获取唯一的id
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID));
            //通过id构造Uri
            Uri uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
            openUri(uri,context);
            break;
        }
    }

    private void openUri(Uri uri,Context context) {
        try {
            //从uri构造输入流
            InputStream fis = context.getContentResolver().openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(fis);
        } catch (Exception e) {

        }
    }

}
